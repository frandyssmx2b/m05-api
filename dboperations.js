var config =  require('./dbconfig');
const mysql = require('mysql2'); /* requiere la dependencia mssql */
const { json } = require('body-parser');
const { query } = require('express');
const Connection = require('mysql/lib/Connection');
var resultado = [];
connection = mysql.createConnection(config);

//connection.connect();
//funcion que devuelve todas la cpus
async function getCpus(){
    try{
        let contador = 0;
        //propia
        const sql = "SELECT * FROM cpus";
        const [result] = await connection.promise().query(sql);
        return result;
        }
    catch(error){
        console.log(error);
    }
}

async function getDrives(){
    try{
        //propia
        const sql = "SELECT * FROM drives";
        const [result] = await connection.promise().query(sql);
        return result;
        }
    catch(error){
        console.log(error);
    }
}

async function getHardware(){
    try{
        //propia
        const sql = "SELECT * FROM hardware";
        const [result] = await connection.promise().query(sql);
        return result;
        }
    catch(error){
        console.log(error);
    }
}

module.exports = {
    getCpus:getCpus,
    getDrives:getDrives,
    getHardware:getHardware
}