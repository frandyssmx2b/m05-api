var dbquerys = require("../../dboperations");
const express = require('express');
const router = express.Router();
const { json } = require('body-parser');

//cuando es post el req viene por parametro tmbne
router.get('/',(req,res)=> {
    //res.send('Conexion correcta a Index');
    res.send("Estas en indice");
    
})

router.get('/cpus',(req,res)=> {
    //res.send('Conexion correcta a Index');
    dbquerys.getCpus().then(result => {
        //result = json(result)
        //res.send(result)
        res.json({ "result": result });
    })
})

router.get('/drivers',(req,res)=> {
    dbquerys.getDrives().then(result => {
        //result = json(result)
        //res.send(result)
        res.json({ "result": result });
    })
})


router.get('/hardware',(req,res)=> {
    dbquerys.getHardware().then(result => {
        //result = json(result)
        //res.send(result)
        res.json({ "result": result });
    })
})




/* 
dbquerys.getCpus().then(result => {
    // console.log(result);
    //console.log(result);
}) */

module.exports = router;